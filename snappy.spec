Name:		snappy
Version:	1.2.1
Release:	1
Summary:	A fast compressor/decompressor
License:	BSD
URL:		https://github.com/google/snappy
Source0:	https://github.com/google/snappy/archive/%{version}/%{name}-%{version}.tar.gz

Patch0:		snappy-thirdparty.patch
Patch2:		add-option-to-enable-rtti-set-default-to-current-ben.patch

BuildRequires:	gcc-c++ make gtest-devel cmake

%description
Snappy is a compression/decompression library. It does not aim for maximum compression,
or compatibility with any other compression library; instead, it aims for very high
speeds and reasonable compression.

%package        devel
Summary:        Development files for snappy
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
This package is the development files for snappy.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake -DSNAPPY_ENABLE_RTTI=ON -DCMAKE_CXX_STANDARD=14
%cmake_build

%install
%cmake_install

# create pkgconfig file
mkdir -p %{buildroot}%{_libdir}/pkgconfig
cat << EOF >%{buildroot}%{_libdir}/pkgconfig/snappy.pc
prefix=%{_prefix}
exec_prefix=%{_exec_prefix}
includedir=%{_includedir}
libdir=%{_libdir}
 
Name: %{name}
Description: A fast compression/decompression library
Version: %{version}
Cflags: -I\${includedir}
Libs: -L\${libdir} -lsnappy
EOF

%check
%ctest

%files
%license COPYING AUTHORS
%{_libdir}/libsnappy.so.*

%files devel
%doc format_*.txt framing_*.txt
%{_includedir}/snappy*.h
%{_libdir}/libsnappy.so
%{_libdir}/pkgconfig/snappy.pc
%{_libdir}/cmake/Snappy

%files help
%doc NEWS README.md

%changelog
* Fri Mar 07 2025 Funda Wang <fundawang@yeah.net> - 1.2.1-1
- update to 1.2.1

* Thu Nov 14 2024 Funda Wang <fundawang@yeah.net> - 1.1.10-3
- adopt to new cmake macro

* Fri Jan 12 2024 zhoupengcheng<zhoupengcheng11@huawei.com> -1.1.10-2
- Build with C++14 instead of C++11; gtest 1.13.0 requires it

* Mon Jul  3 2023 dillon chen<dillon.chen@gmail.com> -1.1.10-1
- update version to 1.1.10
- Removed patch1(inline.patch) as it's no longer required.
- repatch patch2(snappy-stubs-internal.h)

* Wed Jun 22 2022 wangzengliang<wangzengliang1@huawei.com> - 1.1.9-2
- DESC: add option to enable rtti set default to current

* Fri Dec 24 2021 yuanxin<yuanxin24@huawei.com> - 1.1.9-1
- update version to 1.1.9

* Thu Dec 2 2021 hanxinke<hanxinke@huawei.com> - 1.1.8-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC: add testcases and delete unnecessary test code for snappy

* Fri Jul 17 2020 shixuantong <shixuantong> - 1.1.8-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update to 1.1.8-1

* Wed Oct 9 2019 shenyangyang<shenyangyang4@huawei.com> - 1.1.7-10
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:modify the patch number and requires of devel

* Fri Sep 27 2019 shenyangyang<shenyangyang4@huawei.com> - 1.1.7-9
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:move the license

* Tue Sep 24 2019 shenyangyang<shenyangyang4@huawei.com> - 1.1.7-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.7-7
- Package init
